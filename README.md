# SwiftStylable

[![CI Status](http://img.shields.io/travis/Marcel Bloemendaal/SwiftStylable.svg?style=flat)](https://travis-ci.org/Marcel Bloemendaal/SwiftStylable)
[![Version](https://img.shields.io/cocoapods/v/SwiftStylable.svg?style=flat)](http://cocoapods.org/pods/SwiftStylable)
[![License](https://img.shields.io/cocoapods/l/SwiftStylable.svg?style=flat)](http://cocoapods.org/pods/SwiftStylable)
[![Platform](https://img.shields.io/cocoapods/p/SwiftStylable.svg?style=flat)](http://cocoapods.org/pods/SwiftStylable)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SwiftStylable is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "SwiftStylable"
```

## Author

Marcel Bloemendaal, m.bloemendaal@yipyip.nl

## License

SwiftStylable is available under the MIT license. See the LICENSE file for more info.
